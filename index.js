const excelToJson = require("convert-excel-to-json");
const mkdirp = require("mkdirp");
const fs = require("fs");
const _ = require("lodash");

if (!process.argv[2]) {
  console.log("ERROR: Please pass excel file argument");
  process.exit(1);
}
const result = excelToJson({
  sourceFile: process.argv[2] + ".xlsx",
  header: {
    rows: 1
  }
});

const folderName = process.argv[2] + "_build";

mkdirp(`./${folderName}`, err => {
  if (err) console.error(err);
  else {
    let count = 0;
    _.forEach(result["For HTML"], row => {
      const fileName = `${row.A}-${row.B}-${row.C}-${row.D}x${row.E}.html`;
      fs.writeFile(`./${folderName}/${fileName}`, buildHtml(row), err => {
        if (err) console.log(`${fileName} ${err}`);
      });
      count++;
    });
    console.log(`DONE: ${count} html files generated to ${folderName}`);
  }
});

const buildHtml = ({ G, H, I, J }) => {
  const path = _.replace(J, / /g, "+");
  const path1 = _.replace(_.replace(J, / /g, "+"), /\//g, "%3A");

  return `
    <style type="text/css">
    .wrapper {
      display: inline-block;
      position: relative;
    }
    .wrapper a img {
      display: block;
    }
    
    .sr-only {
      position: absolute;
      width: 1px;
      height: 1px;
      margin: -1px;
      padding: 0;
      overflow: hidden;
      clip: rect(0, 0, 0, 0);
      border: 0;
    }
    </style>

    <div class="wrapper">
        <a href="${G}">
            <img src="https://lowes-severe-weather.s3.amazonaws.com/images/${path1}" alt="Learn More">
            <span class="sr-only">${H} ${I}</span>
        </a>
    </div>
    `;
};
